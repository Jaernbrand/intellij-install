
INSTALL_DIR = "/usr/local/bin"
EXEC = "intellij-install"

.PHONY: install
install:
	cp $(EXEC) $(INSTALL_DIR)

.PHONY: uninstall
uninstall:
	rm -f $(INSTALL_DIR)/$(EXEC)

