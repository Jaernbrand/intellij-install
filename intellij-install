#!/bin/bash

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# intellij-install 
# ================
#
# version 1.0.1
#
# Installs Intellij from the given `tar`. The arguments `install_dir` and 
# `desktop_dir` are optional and default values will be used if none are given. 
# A desktop file is generated and installed in `desktop_dir`.
# ```
# intellij-install tar [install_dir [desktop_dir]]
# ```
#
# Arguments
#   * tar : the tar containing the intellij binary
#   * install_dir : the directory in which to install Intellij
#   * desktop_dir : the directory in which to install the desktop file
#
# The default installation location will require `intellij-install` to run with 
# `sudo`.
# ```
# sudo intellij-install ideaIC-2018.1.4.tar.gz
# ```
#
# The optional arguments can be used to install Intellij for the user only.
# ```
# intellij-install ideaIC-2018.1.4.tar.gz $HOME/intellij $HOME/.local/share/applications
# ```
#
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


set -e

if [ -z $1 ]
then
	echo "Missing argument, excpected Intellij tar as first argument" >&2
	exit 1
fi

intellij_tar=$1

if [ -z $2 ]
then
	install_dir="/opt/intellij"
else
	install_dir=$2
fi

if [ -z $3 ]
then
	desktop_dir="/usr/share/applications"
else
	desktop_dir=$3
fi

if [ -d "$install_dir" ]
then
    read -p "$install_dir already exists. Remove $install_dir recursively before install? (y/n/cancel) " answer
    case $answer in
        [Yy]* )
            rm -rf $install_dir
            ;;
        [Nn]* )
            echo "Installing into existing directory $install_dir"
            ;;
        *)
            echo "Canceling installation..."
            exit 0
            ;;
    esac
fi

mkdir -p $install_dir
tar -vxzf $intellij_tar --strip 1 -C $install_dir

mkdir -p $desktop_dir
cat > $desktop_dir/intellij-ce.desktop <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=IntelliJ IDEA Community Edition
Icon=$install_dir/bin/idea.png
Exec="$install_dir/bin/idea.sh" %f
Comment=IntelliJ IDEA Community Edition 
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-idea-ce
EOF

echo "Intellij installed at $install_dir"
echo "DONE"

