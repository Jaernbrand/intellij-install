# intellij-install
Intellij installation script for Linux

## Installation

Install the script globally. This will install the script in `/usr/local/bin`.
```
sudo make install
```

Uninstall the script, if it was installed via the install target above.
```
sudo make uninstall
```

## Usage

Installs Intellij from the given `tar`. The arguments `install_dir` and 
`desktop_dir` are optional and default values will be used if none are given. 
A desktop file is generated and installed in `desktop_dir`.
```
intellij-install tar [install_dir [desktop_dir]]
```

The default installation location will require `intellij-install` to run with 
`sudo`.
```
sudo intellij-install ideaIC-2018.1.4.tar.gz
```

The optional arguments can be used to install Intellij for the user only.
```
intellij-install ideaIC-2018.1.4.tar.gz $HOME/intellij $HOME/.local/share/applications
```
